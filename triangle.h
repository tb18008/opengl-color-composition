//
// Created by User on 02.04.2021.
//

#ifndef COLOR_COMPONENTS_TRIANGLE_H
#define COLOR_COMPONENTS_TRIANGLE_H

#include "vector3.h"
#include "color.h"
#include <iostream>
#include <GL/freeglut.h>

class Triangle {
    public:
        explicit Triangle();
        explicit Triangle(Vector3 *_corners);

    private:
        Vector3 corners[3];
        Vector3 normal;
        void calculateNormal();
        void drawTriangle();
};


#endif //COLOR_COMPONENTS_TRIANGLE_H
