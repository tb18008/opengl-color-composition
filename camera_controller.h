//
// Created by User on 30.03.2021.
//

#ifndef COLOR_COMPONENTS_CAMERA_CONTROLLER_H
#define COLOR_COMPONENTS_CAMERA_CONTROLLER_H

#include "vector3.h"
#include <GL/freeglut.h>

//Structs

struct Vector2 {
    float x;
    float y;
};

//Vector variables
extern Vector3 initial_position;    //The vector from object to the camera
extern Vector3 object_position, camera_projection_xz, camera_projection_yz, camera_normal;

//Mouse event variables
extern bool mouse_dragging;
extern float rotation_sensitivity;

extern struct Vector2 mouse_position;
extern struct Vector2 mouse_motion;
extern struct Vector2 rotation_angle;

//Methods

void setUpCamera(Vector3 _initial_position = Vector3());

void calculateCameraNormal();
void updatePosition();

void mouseEvent(int button, int state, int mouse_x, int mouse_y);
void rotateCamera(int mouse_x, int mouse_y);


#endif //COLOR_COMPONENTS_CAMERA_CONTROLLER_H
