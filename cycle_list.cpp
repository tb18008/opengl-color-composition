//
// Created by User on 02.04.2021.
//

#include "cycle_list.h"

CycleList::CycleList(int values[], int _size) {

    size = _size;

    ListNode *current_node = start = new ListNode{values[0]};

    for (int i = 1; i < size; ++i) {
        current_node->next = new ListNode{values[i]}; //Create new node with value from array
        current_node = current_node->next;  //Change pointer to point to the new node
    }

    //Link last node to start node
    current_node->next = start;

    //Set pointer to start
    ptr = start;
}

int CycleList::getNext() {
    int pointer_value = ptr->value;
    ptr = ptr->next;
    return pointer_value;
}

void CycleList::print() {
    ListNode *current_node = start;
    do {
        cout << current_node->value << endl;
        current_node = current_node->next;
    }
    while (current_node->next != start);
    cout << current_node->value << endl;
}

CycleList::CycleList() {

}
