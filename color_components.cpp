/*! \file    color_components.cpp

    This program is a test harness for the various shapes
    in OpenGLUT.  It may also be useful to see which
    parameters control what behavior in the OpenGLUT
    objects.

    Spinning wireframe and solid-shaded shapes are
    displayed.  Some parameters can be adjusted.

   Keys:
      -    <tt>Esc &nbsp;</tt> Quit
      -    <tt>q Q &nbsp;</tt> Quit
      -    <tt>i I &nbsp;</tt> Show info
      -    <tt>p P &nbsp;</tt> Toggle perspective or orthographic projection
      -    <tt>r R &nbsp;</tt> Toggle fixed or animated rotation around model X-axis
      -    <tt>n N &nbsp;</tt> Toggle visualization of object's normal vectors

    \author  Written by Nigel Stewart November 2003

    \author  Portions Copyright (C) 2004, the OpenGLUT project contributors. <br>
             OpenGLUT branched from freeglut in February, 2004.

    \image   html openglut_shapes.png OpenGLUT Geometric Shapes Demonstration
    \include demos/shapes/shapes.c
*/

#include <GL/freeglut.h>

#include <iostream>
#include <string>
#include <map>

#include <cstdarg>
#include <cstdio>
#include <cstdlib>

#include "image.h"
#include "vector3.h"
#include "camera_controller.h"
#include "quad.h"
#include "triangle.h"
#include "cycle_list.h"

using namespace std;

#ifdef _MSC_VER
/* DUMP MEMORY LEAKS */
#include <crtdbg.h>
#endif

/*
 * OpenGL 2+ shader mode needs some function and macro definitions,
 * avoiding a dependency on additional libraries like GLEW or the
 * GL/glext.h header
 */
#ifndef GL_FRAGMENT_SHADER
#define GL_FRAGMENT_SHADER 0x8B30
#endif

#ifndef GL_VERTEX_SHADER
#define GL_VERTEX_SHADER 0x8B31
#endif

#ifndef GL_COMPILE_STATUS
#define GL_COMPILE_STATUS 0x8B81
#endif

#ifndef GL_LINK_STATUS
#define GL_LINK_STATUS 0x8B82
#endif

#ifndef GL_INFO_LOG_LENGTH
#define GL_INFO_LOG_LENGTH 0x8B84
#endif

typedef char ourGLchar;

#ifndef APIENTRY
#define APIENTRY
#endif

#ifndef GL_VERSION_2_0
typedef GLuint (APIENTRY *PFNGLCREATESHADERPROC) (GLenum type);
typedef void (APIENTRY *PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const ourGLchar **string, const GLint *length);
typedef void (APIENTRY *PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef GLuint (APIENTRY *PFNGLCREATEPROGRAMPROC) (void);
typedef void (APIENTRY *PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (APIENTRY *PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (APIENTRY *PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (APIENTRY *PFNGLGETSHADERIVPROC) (GLuint shader, GLenum pname, GLint *params);
typedef void (APIENTRY *PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, ourGLchar *infoLog);
typedef void (APIENTRY *PFNGLGETPROGRAMIVPROC) (GLenum target, GLenum pname, GLint *params);
typedef void (APIENTRY *PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei *length, ourGLchar *infoLog);
typedef GLint (APIENTRY *PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const ourGLchar *name);
typedef GLint (APIENTRY *PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const ourGLchar *name);
typedef void (APIENTRY *PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (APIENTRY *PFNGLUNIFORMMATRIX3FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
#endif

PFNGLCREATESHADERPROC gl_CreateShader;
PFNGLSHADERSOURCEPROC gl_ShaderSource;
PFNGLCOMPILESHADERPROC gl_CompileShader;
PFNGLCREATEPROGRAMPROC gl_CreateProgram;
PFNGLATTACHSHADERPROC gl_AttachShader;
PFNGLLINKPROGRAMPROC gl_LinkProgram;
PFNGLUSEPROGRAMPROC gl_UseProgram;
PFNGLGETSHADERIVPROC gl_GetShaderiv;
PFNGLGETSHADERINFOLOGPROC gl_GetShaderInfoLog;
PFNGLGETPROGRAMIVPROC gl_GetProgramiv;
PFNGLGETPROGRAMINFOLOGPROC gl_GetProgramInfoLog;
PFNGLGETATTRIBLOCATIONPROC gl_GetAttribLocation;
PFNGLGETUNIFORMLOCATIONPROC gl_GetUniformLocation;
PFNGLUNIFORMMATRIX4FVPROC gl_UniformMatrix4fv;
PFNGLUNIFORMMATRIX3FVPROC gl_UniformMatrix3fv;

void initExtensionEntries(void)
{
    gl_CreateShader = (PFNGLCREATESHADERPROC) glutGetProcAddress ("glCreateShader");
    gl_ShaderSource = (PFNGLSHADERSOURCEPROC) glutGetProcAddress ("glShaderSource");
    gl_CompileShader = (PFNGLCOMPILESHADERPROC) glutGetProcAddress ("glCompileShader");
    gl_CreateProgram = (PFNGLCREATEPROGRAMPROC) glutGetProcAddress ("glCreateProgram");
    gl_AttachShader = (PFNGLATTACHSHADERPROC) glutGetProcAddress ("glAttachShader");
    gl_LinkProgram = (PFNGLLINKPROGRAMPROC) glutGetProcAddress ("glLinkProgram");
    gl_UseProgram = (PFNGLUSEPROGRAMPROC) glutGetProcAddress ("glUseProgram");
    gl_GetShaderiv = (PFNGLGETSHADERIVPROC) glutGetProcAddress ("glGetShaderiv");
    gl_GetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC) glutGetProcAddress ("glGetShaderInfoLog");
    gl_GetProgramiv = (PFNGLGETPROGRAMIVPROC) glutGetProcAddress ("glGetProgramiv");
    gl_GetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC) glutGetProcAddress ("glGetProgramInfoLog");
    gl_GetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC) glutGetProcAddress ("glGetAttribLocation");
    gl_GetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC) glutGetProcAddress ("glGetUniformLocation");
    gl_UniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC) glutGetProcAddress ("glUniformMatrix4fv");
    gl_UniformMatrix3fv = (PFNGLUNIFORMMATRIX3FVPROC) glutGetProcAddress ("glUniformMatrix3fv");
    if (!gl_CreateShader || !gl_ShaderSource || !gl_CompileShader || !gl_CreateProgram || !gl_AttachShader || !gl_LinkProgram || !gl_UseProgram || !gl_GetShaderiv || !gl_GetShaderInfoLog || !gl_GetProgramiv || !gl_GetProgramInfoLog || !gl_GetAttribLocation || !gl_GetUniformLocation || !gl_UniformMatrix4fv || !gl_UniformMatrix3fv)
    {
        fprintf (stderr, "glCreateShader, glShaderSource, glCompileShader, glCreateProgram, glAttachShader, glLinkProgram, glUseProgram, glGetShaderiv, glGetShaderInfoLog, glGetProgramiv, glGetProgramInfoLog, glGetAttribLocation, glGetUniformLocation, glUniformMatrix4fv or gl_UniformMatrix3fv not found");
        exit(1);
    }
}

/*
 * These global variables control which object is drawn,
 * and how it is drawn.  No object uses all of these
 * variables.
 */
static GLboolean show_info = GL_TRUE;
static float ar;
static GLboolean persProject = GL_TRUE;
static GLboolean visNormals  = GL_FALSE;
static Image image;

CycleList channel_cycle;
char current_channel;
map<char, char * const> channel_names;

static void drawSolidMesh(void);
void drawQuads(Vector3 *corners, int pixel_x, int pixel_y);
void drawTriangles(int pixel_x, int pixel_y);
void changeChannel();

struct Offset2{
    float x;
    float z;
};

static void drawSolidMesh()
{

    Quad quad;
    Vector3 corners[4];

    switch (image.getDimensionType()) {
        case 0:
            //Draw a quad the one value of the only pixel
            drawQuads(corners, 0, 0);
            break;
        case 1:
            //Draw a quad for each of the pixels in the row
            for (int i = 0; i < image.width - 1; ++i) drawQuads(corners, i, 0);
            break;

        case 2:
            //Draw a quad for each of the pixels in the column
            for (int i = 0; i < image.height - 1; ++i) drawQuads(corners, 0, i);
            break;
        case 3:
            //Draw a 4 triangles to make a quad for each pixel except the last row and column
            for (int i = 0; i < image.width - 1; ++i) {
                for (int j = 0; j < image.height - 1; ++j) drawTriangles(i, j);
            }
            break;
    }
}

void drawQuads(Vector3 *corners, int pixel_x, int pixel_y){
    float color_value;
    float scale;
    int number_of_quads;

    Quad quad;

    //Calculate value and color based on current channel
    color_value = image.getValueAt(pixel_x,pixel_y,current_channel);
    switch (current_channel) {
        case ('v'):
            glColor3d(color_value,color_value,color_value);
            break;
        case ('r'):
            glColor3d(color_value,0.0,0.0);
            break;
        case ('g'):
            glColor3d(0.0,color_value,0.0);
            break;
        case ('b'):
            glColor3d(0.0,0.0,color_value);
            break;
        case ('a'):
            glColor3d(color_value,color_value,color_value);
            break;
    }
    //Scale for mesh Y axis
    color_value = color_value * 2 - 1;


    switch (image.getDimensionType()) {
        case 0:

            corners[0] = Vector3(-1,color_value,1);
            corners[1] = Vector3(1,color_value,1);
            corners[2] = Vector3(1,color_value,-1);
            corners[3] = Vector3(-1,color_value,-1);
            break;

        case 1:

            scale = 1 / (float)image.width;
            number_of_quads = (image.width - 1);

            corners[0] = Vector3( (float)pixel_x * 2 / number_of_quads - 1, color_value, -scale );
            corners[1] = Vector3( (float)pixel_x * 2 / number_of_quads - 1, color_value, scale );

            color_value = image.getValueAt(pixel_x + 1,0,current_channel) * 2 - 1;

            corners[2] = Vector3( (float)(pixel_x + 1) * 2 / number_of_quads - 1,color_value, scale );
            corners[3] = Vector3( (float)(pixel_x + 1) * 2 / number_of_quads - 1, color_value, -scale );
            break;

        case 2:

            scale = 1 / (float)image.height;
            number_of_quads = (image.height - 1);

            corners[0] = Vector3( scale , color_value, (float)pixel_y * 2 / number_of_quads - 1);
            corners[1] = Vector3( -scale, color_value, (float)pixel_y * 2 / number_of_quads - 1);

            color_value = image.getValueAt(0,pixel_y + 1,current_channel) * 2 - 1;

            corners[2] = Vector3( -scale, color_value, (float)(pixel_y + 1) * 2 / number_of_quads - 1);
            corners[3] = Vector3( scale, color_value, (float)(pixel_y + 1) * 2 / number_of_quads - 1);
            break;
    }

    quad = Quad(corners);
}

void drawTriangles(int pixel_x, int pixel_y){

    Vector3 corners[3];
    Vector3 vertices[5];

    if(image.getDimensionType() != 3) return;

    float color_value[5];
    float scale;
    int number_of_quads;

    Triangle triangle;

    Offset2 offset {-1, -1};

    //Finding largest side and setting scale accordingly
    if(image.width > image.height) {
        number_of_quads = image.width - 1;
        //Shift away from top since image is wider than higher
        offset.z += (float)(image.width - image.height) / image.width;
    }
    else {
        number_of_quads = image.height - 1;
        //Shift away from left since image is higher than wider
        offset.x += (float)(image.height - image.width) / image.height;
    }
    scale = 2 / ((float)number_of_quads);

    /*
     * 0     1
     *    4
     * 2     3
     */

    //Calculate values
    color_value[0] = image.getValueAt(pixel_x,pixel_y,current_channel) * 2 - 1; //Upper left
    color_value[1] = image.getValueAt(pixel_x + 1,pixel_y,current_channel) * 2 - 1; //Upper right
    color_value[2] = image.getValueAt(pixel_x,pixel_y + 1,current_channel) * 2 - 1; //Lower left
    color_value[3] = image.getValueAt(pixel_x + 1,pixel_y + 1,current_channel) * 2 - 1; //Lower right
    color_value[4] = (((color_value[0] + color_value[3]) / 2) + ((color_value[1] + color_value[2]) / 2)) / 2;   //Middle


    float average_color = (color_value[4] + 1) / 2;

    switch (current_channel) {
        case ('v'):
            glColor3d(average_color,average_color,average_color);
            break;
        case ('r'):
            glColor3d(average_color,0.0,0.0);
            break;
        case ('g'):
            glColor3d(0.0,average_color,0.0);
            break;
        case ('b'):
            glColor3d(0.0,0.0,average_color);
            break;
        case ('a'):
            glColor3d(average_color,average_color,average_color);
            break;
    }

    //Calculate vertices
    vertices[0] = Vector3(pixel_x * scale + offset.x,color_value[0],pixel_y * scale + offset.z); //Upper left
    vertices[1] = Vector3((pixel_x + 1) * scale + offset.x,color_value[1],pixel_y * scale + offset.z); //Upper right
    vertices[2] = Vector3(pixel_x * scale + offset.x,color_value[2],(pixel_y + 1) * scale + offset.z); //Lower left
    vertices[3] = Vector3((pixel_x + 1) * scale + offset.x,color_value[3],(pixel_y + 1) * scale + offset.z); //Lower right
    vertices[4] = Vector3((pixel_x + 0.5) * scale + offset.x,color_value[4],(pixel_y + 0.5) * scale + offset.z);   //Middle

    /*
     * 0---2
     * |\ /|
     * | 1 |
     * |/ \|
     * +---+
     */

    //Draw upper triangle
    corners[0] = vertices[0];
    corners[1] = vertices[4];
    corners[2] = vertices[1];
    triangle = Triangle(corners);

    /*
     * 0---+
     * |\ /|
     * | 2 |
     * |/ \|
     * 1---+
     */

    //Draw left triangle
    corners[0] = vertices[0];
    corners[1] = vertices[2];
    corners[2] = vertices[4];
    triangle = Triangle(corners);

    /*
     * +---+
     * |\ /|
     * | 2 |
     * |/ \|
     * 0---1
     */

    //Draw bottom triangle
    corners[0] = vertices[2];
    corners[1] = vertices[3];
    corners[2] = vertices[4];
    triangle = Triangle(corners);

    /*
     * +---1
     * |\ /|
     * | 2 |
     * |/ \|
     * +---0
     */

    //Draw right triangle
    corners[0] = vertices[3];
    corners[1] = vertices[1];
    corners[2] = vertices[4];
    triangle = Triangle(corners);


}

static void drawAxis(){

    //Draw positive arrows

    //Red
    glColor3d(1,0,0);
    glBegin( GL_TRIANGLES );
    glNormal3d(0.0, 1.0, 0.0);  glVertex3d(1.1,-1.0,-1.0); glVertex3d(1.1,-1.0,1.0); glVertex3d(1.2,-1.0,0.0);
    glEnd();

    //Green
//    glColor3d(0,1,0);
//    glBegin( GL_TRIANGLES );
//    glNormal3d(0.0, 0.0, 1.0); glVertex3d(-1.0,1.1,0.0); glVertex3d(1.0,1.1,0.0); glVertex3d(0.0,1.2,0.0);
//    glEnd();

    //Blue
    glColor3d(0,0,1);
    glBegin( GL_TRIANGLES );
    glNormal3d(0.0, 1.0, 0.0);  glVertex3d(1.0,-1.0,1.1); glVertex3d(-1.0,-1.0,1.1); glVertex3d(0.0,-1.0,1.2);
    glEnd();

    //Draw negative arrows

    //Red
    glColor3d(1,0,0);
    glBegin( GL_LINE_LOOP );
    glNormal3d(0.0, 1.0, 0.0);  glVertex3d(-1.1,-1.0,1.0); glVertex3d(-1.1,-1.0,-1.0); glVertex3d(-1.2,-1.0,0.0);
    glEnd();

    //Green
    glColor3d(0,1,0);
    glBegin( GL_LINE_LOOP );
    glNormal3d(0.0, 0.0, 1.0); glVertex3d(1.0,-1.1,0.0); glVertex3d(-1.0,-1.1,0.0); glVertex3d(0.0,-1.2,0.0);
    glEnd();

    //Blue
    glColor3d(0,0,1);
    glBegin( GL_LINE_LOOP );
    glNormal3d(0.0, 1.0, 0.0);  glVertex3d(-1.0,-1.0,-1.1); glVertex3d(1.0,-1.0,-1.1); glVertex3d(0.0,-1.0,-1.2);
    glEnd();

};

/*!
    Does printf()-like work using freeglut
    glutBitmapString().  Uses a fixed font.  Prints
    at the indicated row/column position.

    Limitation: Cannot address pixels.
    Limitation: Renders in screen coords, not model coords.
*/
static void shapesPrintf (int row, int col, const char *fmt, ...)
{
    static char buf[256];
    int viewport[4];
    void *font = GLUT_BITMAP_9_BY_15;
    va_list args;

    va_start(args, fmt);
#if defined(WIN32) && !defined(__CYGWIN__)
    (void) _vsnprintf (buf, sizeof(buf), fmt, args);
#else
    (void) vsnprintf (buf, sizeof(buf), fmt, args);
#endif
    va_end(args);

    glGetIntegerv(GL_VIEWPORT,viewport);

    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

        glOrtho(0,viewport[2],0,viewport[3],-1,1);

        glRasterPos2i
        (
              glutBitmapWidth(font, ' ') * col,
            - glutBitmapHeight(font) * row + viewport[3]
        );
        glutBitmapString (font, (unsigned char*)buf);

    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

static void drawInfo()
{
    int row = 1;
    if (persProject)
        shapesPrintf (row++, 1, "Perspective projection (p)");
    else
        shapesPrintf (row++, 1, "Orthographic projection (p)");

    shapesPrintf (row++, 1, "Channel: %s (c)", channel_names.at(current_channel));
    shapesPrintf (row++, 1, "Rotation angle: [%5.2f, %5.2f]",rotation_angle.x,rotation_angle.y);
    shapesPrintf (row++, 1, "Camera position: (%5.2f, %5.2f, %5.2f)", -object_position.x, -object_position.y, -object_position.z);
}

/* GLUT callback Handlers */
static void
resize(int width, int height)
{
    ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
}

/*
 * Contains the display of 3D object and 2D information text
 * as well as the color, translation of the 3D object.
 */

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glutSetOption(GLUT_GEOMETRY_VISUALIZE_NORMALS,visNormals);  /* Normals visualized or not? */

        /* fixed function pipeline */
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        if (persProject)
            glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);
        else
            glOrtho(-ar*3, ar*3, -3.0, 3.0, 2.0, 100.0);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glEnable(GL_LIGHTING);

        glRotated(rotation_angle.x,0,1,0);    //Rotate objects around Y axis
        glRotated(rotation_angle.y,camera_normal.x,0,camera_normal.z);

        glTranslated(object_position.x, object_position.y, object_position.z);

        drawSolidMesh();
        drawAxis();

        glDisable(GL_LIGHTING);
        glColor3d(1,0.75,0.5);



    drawInfo();

    glutSwapBuffers();
}

static void
key(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27 :
    case 'Q':
    case 'q': glutLeaveMainLoop () ;      break;

    case 'I':
    case 'i': show_info=!show_info;       break;

    case 'P':
    case 'p': persProject=!persProject;   break;

    case 'C':
    case 'c': changeChannel();      break;

    default:
        break;
    }

    glutPostRedisplay();
}

void changeChannel(){
    current_channel = (char)channel_cycle.getNext();
}


static void
idle(void)
{
    glutPostRedisplay();
}


/* Program entry point */

int main(int argc, char *argv[])
{
    string file_path;

    if (argc > 1) file_path = argv[1];
    else
    {
        cout << "Selected image file path: ";
        getline (cin, file_path);
    }

    image = Image(file_path);

    cout << "Image width = " << image.width << '\n';
    cout << "Image height = " << image.height << '\n';

    int channels[] = {(int)'v', (int)'r', (int)'g', (int)'b', (int)'a'};

    channel_names.insert(pair<char, char * const>('v', strdup("Value")));
    channel_names.insert(pair<char, char * const>('r', strdup("Red")));
    channel_names.insert(pair<char, char * const>('g', strdup("Green")));
    channel_names.insert(pair<char, char * const>('b', strdup("Blue")));
    channel_names.insert(pair<char, char * const>('a', strdup("Alpha")));

    channel_cycle = CycleList(reinterpret_cast<int *>(channels), 5);
    current_channel = channel_cycle.getNext();



    setUpCamera(Vector3(1, 1, 1).normalize() * 5);

    glutInitWindowSize(800,600);
    glutInitWindowPosition(40,40);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

    glutCreateWindow("Color composition");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutIdleFunc(idle);

    glutSetOption ( GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION ) ;

    glClearColor(0.1,0.1,0.1,1);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);

    initExtensionEntries();

    glutMouseFunc(mouseEvent);
    glutMotionFunc(rotateCamera);

    glutMainLoop();

#ifdef _MSC_VER
    /* DUMP MEMORY LEAK INFORMATION */
    _CrtDumpMemoryLeaks () ;
#endif

    return EXIT_SUCCESS;
}
