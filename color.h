//
// Created by User on 27.03.2021.
//

#ifndef COLOR_COMPONENTS_COLOR_H
#define COLOR_COMPONENTS_COLOR_H

#include <iostream>
using namespace std;

class Color {
    public:
        int r, g, b, a;
        explicit Color(unsigned char* start_ptr_red);
        friend ostream& operator<<(ostream& os, Color color);
};


#endif //COLOR_COMPONENTS_COLOR_H
