//
// Created by User on 27.03.2021.
//

#ifndef COLOR_COMPONENTS_IMAGE_H
#define COLOR_COMPONENTS_IMAGE_H

#include <vector>
#include <string>
#include <iostream>
#include "color.h"

class Image {
    public:

        int width = 0, height = 0;

        std::vector<Color*> pixels;    //RGBa data 0 - 255 for each of 4 color channels
        unsigned char* data;    //Data for texture loading

        bool size_flags[4] = {false, false, false, false};
        /*
         * 0 => 1 x 1 pixels
         * 1 => 1 x height pixels
         * 2 => width x 1 pixels
         * 3 => width x height pixels
         */

        explicit Image(const std::string& file_path); // Constructor with specified file path
        explicit Image(); // Constructor without params
        int getDimensionType();

        float getValueAt(int x, int y, char channel);
        Color* getPixelAt(int x, int y);
        void printPixels();

    private:

        // Loads as RGBA... even if file is only RGB
        // Feel free to adjust this if you so please, by changing the 4 to a 0.
        bool load_image(const std::string& file_path);
};

#endif //COLOR_COMPONENTS_IMAGE_H
