//
// Created by User on 02.04.2021.
//

#ifndef COLOR_COMPONENTS_QUAD_H
#define COLOR_COMPONENTS_QUAD_H


#include "vector3.h"
#include "color.h"
#include <iostream>
#include <GL/freeglut.h>

class Quad {
    public:
        explicit Quad();
        explicit Quad(Vector3 *_corners);

    private:
        Vector3 corners[4];
        Vector3 normal;
        void calculateNormal();
        void drawQuad();

};


#endif //COLOR_COMPONENTS_QUAD_H
