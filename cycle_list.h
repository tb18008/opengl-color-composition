//
// Created by User on 02.04.2021.
//

#ifndef COLOR_COMPONENTS_CYCLE_LIST_H
#define COLOR_COMPONENTS_CYCLE_LIST_H

#include <iostream>

using namespace std;

class CycleList {

    public:

        //Struct
        struct ListNode {
            int value;
            ListNode *next;
            ListNode(int x) : value(x), next(nullptr) {}
            ListNode(int x, ListNode *next_node) : value(x), next(next_node) {}
        };

        //Constructor
        explicit CycleList();
        CycleList(int *values, int _size);

        //Methods
        int getNext();
        void print();

    private:
        int size;
        ListNode *start;
        ListNode *ptr;
};

#endif //COLOR_COMPONENTS_CYCLE_LIST_H
