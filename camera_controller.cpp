//
// Created by User on 30.03.2021.
//

#include "camera_controller.h"

//Vector variables
Vector3 initial_position, object_position, camera_projection_xz, camera_projection_yz, camera_normal;

//Mouse event variables
bool mouse_dragging = false;
float rotation_sensitivity = 0.3;

struct Vector2 mouse_position;
struct Vector2 mouse_motion;
struct Vector2 rotation_angle;

/*
 *  Calculate the vector perpendicular to Vector3 UP and the projection of vector going from world origin to camera position
 */

void setUpCamera(Vector3 _initial_position){

    //Camera position doesn't depend on _initial_position but the rotation angle does
    Vector3 backward = Vector3::BACKWARD;
    Vector3 right = Vector3::RIGHT;
    Vector3 forward = Vector3::FORWARD;

    //Calculate initial rotation angle
    camera_projection_yz = camera_projection_xz = _initial_position;    //Set the two projection variables
    camera_projection_xz.y = camera_projection_yz.x = 0;    //Project onto respective axis

    if(camera_projection_xz.z > 0) rotation_angle.x = 360 - camera_projection_xz.angle(right);
    else rotation_angle.x = camera_projection_xz.angle(right);
    rotation_angle.y = camera_projection_yz.angle(forward);

    //Zoom out from the world origin according to the length of the initial position vector
    initial_position = backward * _initial_position.magnitude();

    //Set camera position variables
    updatePosition();
}

void calculateCameraNormal() {
    //Set the projection based on the camera position
    camera_projection_xz = object_position;
    camera_projection_xz.y = 0;
    camera_projection_xz.normalize();

    //Calculate the vector perpendicular to the camera projection and the vector UP
    camera_normal = camera_projection_xz.cross(const_cast<Vector3 &>(Vector3::UP)).normalize();
}

void mouseEvent(int button, int state, int mouse_x, int mouse_y)
{
    if(button == GLUT_LEFT_BUTTON){
        if(state == GLUT_DOWN) {
            mouse_dragging = true;
            mouse_position.x = mouse_x;
            mouse_position.y = mouse_y;
        }
        if(state == GLUT_UP) {
            mouse_dragging = false;
            mouse_motion.x = 0;
            mouse_motion.y = 0;
        }
    }

}

void rotateCamera(int mouse_x, int mouse_y){
    if (mouse_dragging) {

        //Calculate mouse motion on both axis
        mouse_motion.x = mouse_x - mouse_position.x;
        mouse_motion.y = mouse_y - mouse_position.y;

        //Set new mouse position
        mouse_position.x = mouse_x;
        mouse_position.y = mouse_y;

        //Set angle
        rotation_angle.x += mouse_motion.x * rotation_sensitivity;

        int angle_after_increment = rotation_angle.y + mouse_motion.y * rotation_sensitivity;
        if(angle_after_increment < 90 && angle_after_increment > 0){
            rotation_angle.y += mouse_motion.y * rotation_sensitivity;
        }

        updatePosition();
    }
}

void updatePosition(){

    //Set new camera position after rotating around Vector3::UP
    object_position = initial_position.rotateAroundAxis(Vector3::UP, rotation_angle.x);

    //Calculate the normal of the camera after the rotation
    calculateCameraNormal();

    //Redo the rotation around Vector3::UP and the camera normal, since camera normal is updated
    object_position = initial_position.rotateAroundAxis(Vector3::UP, rotation_angle.x).rotateAroundAxis(Vector3(camera_normal.x, 0, camera_normal.z), rotation_angle.y);

}
