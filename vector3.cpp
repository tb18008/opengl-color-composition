//
// Created by User on 28.03.2021.
//

#include "vector3.h"
#include <iostream>

const Vector3 Vector3::UP = Vector3(0, 1, 0);
const Vector3 Vector3::DOWN = Vector3(0, -1, 0);
const Vector3 Vector3::RIGHT = Vector3(1, 0, 0);
const Vector3 Vector3::LEFT = Vector3(-1, 0, 0);
const Vector3 Vector3::FORWARD = Vector3(0, 0, 1);
const Vector3 Vector3::BACKWARD = Vector3(0, 0, -1);

Vector3::Vector3(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

float Vector3::dot(Vector3 &vector_b) {
    return x * vector_b.x + y * vector_b.y + z * vector_b.z;
}

Vector3 Vector3::cross(Vector3 &vector_b) {
    return Vector3(y * vector_b.z-z * vector_b.y, z * vector_b.x-x * vector_b.z, x * vector_b.y-y * vector_b.x);
}

float Vector3::angle(Vector3 &vector_b){
    return acos(this->dot(vector_b) / (this->magnitude() * vector_b.magnitude())) * 180.0 / M_PI;
}

Vector3 &Vector3::normalize() {
    float invLength = 1.0f / this->magnitude();
    x *= invLength;
    y *= invLength;
    z *= invLength;
    return *this;
}

float Vector3::magnitude() {
    return sqrtf(x*x + y*y + z*z);
}

//Scaling a vector by a float
Vector3 operator*(float scale, Vector3 vector) {
    return Vector3(scale * vector.x, scale * vector.y, scale * vector.z);
}

Vector3 operator*(Vector3 vector, float scale) {
    return Vector3(scale * vector.x, scale * vector.y, scale * vector.z);
}

//Adding two vectors
Vector3 operator+(Vector3 vector_a, Vector3 vector_b) {
    return Vector3(vector_a.x + vector_b.x, vector_a.y + vector_b.y, vector_a.z + vector_b.z);
}

//Subtracting one vector from another
Vector3 operator-(Vector3 vector_a, Vector3 vector_b) {
    return Vector3(vector_a.x - vector_b.x, vector_a.y - vector_b.y, vector_a.z - vector_b.z);
}

//Converting vector to stream <<
ostream &operator<<(ostream &os, Vector3 vector) {
    os << "Vector3(" << round(vector.x * 100) / 100 << ", " << round(vector.y * 100)  / 100 << ", " << round(vector.z * 100) / 100 << ')';
    return os;
}

Vector3 Vector3::rotateAroundAxis(Vector3 rotation_axis, float degrees) {
    float radians = -degrees / 180 * M_PI;   // rotation angle as radian

    return *this * cos(radians) + (rotation_axis.cross(*this)) * sin(radians) + rotation_axis * (rotation_axis.dot(*this)) * (1 - cos(radians));
}

void Vector3::invert() {
    this->x = -x;
    this->y = -y;
    this->z = -z;
}

bool operator==(Vector3 vector_a, Vector3 vector_b) {
    return vector_a.x == vector_b.x && vector_a.y == vector_b.y && vector_a.z == vector_b.z;
}

bool operator!=(Vector3 vector_a, Vector3 vector_b) {
    return vector_a.x != vector_b.x || vector_a.y != vector_b.y || vector_a.z != vector_b.z;
}
