//
// Created by User on 27.03.2021.
//

#include "color.h"

Color::Color(unsigned char* start_ptr_red){
    r = start_ptr_red[0];
    g = start_ptr_red[1];
    b = start_ptr_red[2];
    a = start_ptr_red[3];
}

ostream &operator<<(ostream &os, Color color) {
    os << "Color(" << color.r << ", " << color.g << ", " << color.b << ", " << color.a << ')';
    return os;
}
