//
// Created by User on 27.03.2021.
//
#include "image.h"
extern "C" {
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
}
using namespace std;

Image::Image(const string& file_path){
    try {
        //Check if loading the image was successful
        if (!load_image(file_path)) throw std::exception();
        else{
            //Load image successful
            if(width * height == 1) size_flags[0] = true;   // 1 x 1 pixels
            else if(height == 1) size_flags[1] = true;   //width x 1 pixels
            else if(width == 1) size_flags[2] = true;   //1 x height pixels
            else size_flags[3] = true;   //width x height pixels
        }
    }
    catch(int e)
    {
        cout << "Failed to load image " << file_path << endl;
    }
}

Image::Image() = default;

int Image::getDimensionType() {
    if(size_flags[0]) return 0;
    if(size_flags[1]) return 1;
    if(size_flags[2]) return 2;
    else return 3;
}

bool Image::load_image(const string& file_path)
{
    int number_of_channels;

    vector<unsigned char> image;

    unsigned char* data = stbi_load(file_path.c_str(), &width, &height, &number_of_channels, 4);
    //If loading the image was successful write data to image array
    if (data != nullptr) image = vector<unsigned char>(data, data + width * height * 4);

    stbi_image_free(data);

    //Write pixel color data to array
    Image::pixels.reserve(width * height);
    for (auto i = image.begin(); i != image.end(); i +=4) pixels.push_back(new Color(&*i));

    return true;
}

void Image::printPixels() {
    for (int i = 0; i < width; ++i) {
        for (int j = 0; j < height; ++j) {
            cout << "["<<i<<", "<<j<<"] " << *pixels.at(width * j + i) << endl;
        }
    }
}

float Image::getValueAt(int x, int y, char channel = 'v') {

    // Argument validation

    try {
        if(x * height + y > width * height) throw (0);
        if(channel != 'r' && channel != 'g' && channel != 'b' && channel != 'a' && channel != 'v') throw (1);
    }
    catch(int e)
    {
        if (e == 0) cout << "Pixel at " << x << " and " << y << " is out of bounds" << endl;
        if (e == 1) cout << "Channel " << channel << " is invalid" << endl;
    }

    // Return value depending on channel

    Color* pixel = pixels.at(x * height + y);

    switch (channel) {
        case 'r':
            return (float)pixel->r / 255;
        case 'g':
            return (float)pixel->g / 255;
        case 'b':
            return (float)pixel->b / 255;
        case 'a':
            return (float)pixel->a / 255;
        default:
            return (float)(pixel->r + pixel->g + pixel->b) / 765;
    }
}

Color* Image::getPixelAt(int x, int y) {
    return pixels.at(x * height + y);
}

