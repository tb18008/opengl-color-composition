//
// Created by User on 02.04.2021.
//

#include "triangle.h"

Triangle::Triangle() {

}

Triangle::Triangle(Vector3 *_corners) {
    /*
     * ^ y
     * |
     * | 2
     * | | \
     * | 0 - 1
     * +-------> x
     */

    corners[0] = _corners[0];
    corners[1] = _corners[1];
    corners[2] = _corners[2];

    calculateNormal();

    drawTriangle();
}

void Triangle::calculateNormal() {
    Vector3 vector_a = corners[1] - corners[0];
    Vector3 vector_b = corners[2] - corners[0];
    normal = vector_a.cross(vector_b);

}

void Triangle::drawTriangle() {
    glBegin( GL_TRIANGLES );
    glNormal3d( normal.x, normal.y, normal.z );

    glVertex3d(corners[0].x,corners[0].y, corners[0].z);
    glVertex3d(corners[1].x,corners[1].y, corners[1].z);
    glVertex3d(corners[2].x,corners[2].y, corners[2].z);
    glEnd();
}
