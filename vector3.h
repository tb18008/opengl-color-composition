//
// Created by User on 28.03.2021.
//

#ifndef COLOR_COMPONENTS_VECTOR3_H
#define COLOR_COMPONENTS_VECTOR3_H

#include <cmath>       /* sqrt */
#include <string>
#include <sstream>

using namespace std;

class Vector3 {
    public:

        //Variables
        float x, y, z;

        //Constants
        static const Vector3 UP, DOWN, RIGHT, LEFT, FORWARD, BACKWARD;

        //Constructor
        explicit Vector3(float x = 0, float y = 0, float z = 0);

        //Methods
        float dot(Vector3& vector_b);
        Vector3 cross(Vector3& vector_b);
        Vector3& normalize();
        float magnitude();
        Vector3 rotateAroundAxis(Vector3 rotation_axis, float degrees);
        void invert();

        //Operators
        friend Vector3 operator*(float scale, Vector3 vector);
        friend Vector3 operator*(Vector3 vector, float scale);
        friend ostream& operator<<(ostream& os, Vector3 vector);
        friend Vector3 operator+(Vector3 vector_a, Vector3 vector_b);
        friend Vector3 operator-(Vector3 vector_a, Vector3 vector_b);
        friend bool operator==(Vector3 vector_a, Vector3 vector_b);
        friend bool operator!=(Vector3 vector_a, Vector3 vector_b);

    float angle(Vector3 &vector_b);
};



#endif //COLOR_COMPONENTS_VECTOR3_H
